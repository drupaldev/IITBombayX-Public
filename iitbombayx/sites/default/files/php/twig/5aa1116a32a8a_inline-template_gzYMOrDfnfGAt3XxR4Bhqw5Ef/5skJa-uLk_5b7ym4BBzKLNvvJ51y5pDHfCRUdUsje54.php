<?php

/* {# inline_template_start #}<div class="panel panel-default">
<div class="panel-heading">
<img class="img-responsive" src="http://courses.iitbombayx.in{{ field_course_image }}" alt ="" style="height:142px;width:378px;"> 
<div class="course_image_hover"><p> Learn More</p></div>


<p class="clickable_panel">{{ field_course_code }}</p>


</div>

<div class="panel-body custom-panel" style="max-height:225px;">
<p class="course_code">{{ field_course_code }}</p> 

<h4>{{ field_course_name }}</h4>


<div>
<div  class="col-sm-9">

{{ field_time_line }} 

<br>
<h5> Starts :  {{ field_course_start }} </h5>
</div>


<div  class="col-sm-3">
{{ field_mooc_icon }}
 </div>


</div>

</div>



</div>

 */
class __TwigTemplate_a1a268190b1dda62acfbc37eccf8e42401f0565f2e6b6ef43737640db92bdbb1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"panel panel-default\">
<div class=\"panel-heading\">
<img class=\"img-responsive\" src=\"http://courses.iitbombayx.in";
        // line 3
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_course_image"] ?? null), "html", null, true));
        echo "\" alt =\"\" style=\"height:142px;width:378px;\"> 
<div class=\"course_image_hover\"><p> Learn More</p></div>


<p class=\"clickable_panel\">";
        // line 7
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_course_code"] ?? null), "html", null, true));
        echo "</p>


</div>

<div class=\"panel-body custom-panel\" style=\"max-height:225px;\">
<p class=\"course_code\">";
        // line 13
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_course_code"] ?? null), "html", null, true));
        echo "</p> 

<h4>";
        // line 15
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_course_name"] ?? null), "html", null, true));
        echo "</h4>


<div>
<div  class=\"col-sm-9\">

";
        // line 21
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_time_line"] ?? null), "html", null, true));
        echo " 

<br>
<h5> Starts :  ";
        // line 24
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_course_start"] ?? null), "html", null, true));
        echo " </h5>
</div>


<div  class=\"col-sm-3\">
";
        // line 29
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_mooc_icon"] ?? null), "html", null, true));
        echo "
 </div>


</div>

</div>



</div>

";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"panel panel-default\">
<div class=\"panel-heading\">
<img class=\"img-responsive\" src=\"http://courses.iitbombayx.in{{ field_course_image }}\" alt =\"\" style=\"height:142px;width:378px;\"> 
<div class=\"course_image_hover\"><p> Learn More</p></div>


<p class=\"clickable_panel\">{{ field_course_code }}</p>


</div>

<div class=\"panel-body custom-panel\" style=\"max-height:225px;\">
<p class=\"course_code\">{{ field_course_code }}</p> 

<h4>{{ field_course_name }}</h4>


<div>
<div  class=\"col-sm-9\">

{{ field_time_line }} 

<br>
<h5> Starts :  {{ field_course_start }} </h5>
</div>


<div  class=\"col-sm-3\">
{{ field_mooc_icon }}
 </div>


</div>

</div>



</div>

";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 29,  123 => 24,  117 => 21,  108 => 15,  103 => 13,  94 => 7,  87 => 3,  83 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "{# inline_template_start #}<div class=\"panel panel-default\">
<div class=\"panel-heading\">
<img class=\"img-responsive\" src=\"http://courses.iitbombayx.in{{ field_course_image }}\" alt =\"\" style=\"height:142px;width:378px;\"> 
<div class=\"course_image_hover\"><p> Learn More</p></div>


<p class=\"clickable_panel\">{{ field_course_code }}</p>


</div>

<div class=\"panel-body custom-panel\" style=\"max-height:225px;\">
<p class=\"course_code\">{{ field_course_code }}</p> 

<h4>{{ field_course_name }}</h4>


<div>
<div  class=\"col-sm-9\">

{{ field_time_line }} 

<br>
<h5> Starts :  {{ field_course_start }} </h5>
</div>


<div  class=\"col-sm-3\">
{{ field_mooc_icon }}
 </div>


</div>

</div>



</div>

", "");
    }
}
