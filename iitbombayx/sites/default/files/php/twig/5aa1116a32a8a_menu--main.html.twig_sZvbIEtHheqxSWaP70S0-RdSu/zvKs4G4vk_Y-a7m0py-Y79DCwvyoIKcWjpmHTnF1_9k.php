<?php

/* themes/iitbombayx/templates/menu--main.html.twig */
class __TwigTemplate_b9e4a2bd82b78e18f6e5a19bf24aab2afab65ae7b9e0513800eff9c07fe3735d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("import" => 18, "macro" => 26, "if" => 28, "for" => 34, "set" => 36);
        $filters = array();
        $functions = array("link" => 47, "url" => 62);

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('import', 'macro', 'if', 'for', 'set'),
                array(),
                array('link', 'url')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 18
        $context["menus"] = $this;
        // line 19
        echo "
";
        // line 24
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links(($context["items"] ?? null), ($context["attributes"] ?? null), 0, ($context["edx_site_path"] ?? null), ($context["logged_username"] ?? null), ($context["logged_email"] ?? null), ($context["profile_link"] ?? null), ($context["logout_link"] ?? null), ($context["account_link"] ?? null))));
        echo "

";
        // line 99
        echo "

<div id=\"after_login_menu\" style=\"display:none;\">
\t<ul class=\"nav navbar-nav nav-right\">
 \t\t<li class=\"dropdown\">
          \t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t<div class=\"logged_user_dropdown\">
                                <div id='username'>";
        // line 106
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["logged_username"] ?? null), "html", null, true));
        echo "</div>

\t\t\t\t<span class=\"caret\"></span>
\t   \t\t        </div>
                        </a>

       \t\t\t   <ul class=\"dropdown-menu\">
            <li><a href=\"";
        // line 113
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edx_site_path"] ?? null), "html", null, true));
        echo "/dashboard\">Dashboard</a></li>
            <li><a href=\"";
        // line 114
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["account_link"] ?? null), "html", null, true));
        echo "\" id=\"account_link\">Account</a></li>
            <li><a href=\"";
        // line 115
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["profile_link"] ?? null), "html", null, true));
        echo "\" id=\"profile_link\">Profile</a></li>
            <li><a href=\"";
        // line 116
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["logout_link"] ?? null), "html", null, true));
        echo "\" id=\"logout_link\">Logout</a></li>
         \t\t    </ul>
 \t\t</li>

\t</ul>
</div> 

";
    }

    // line 26
    public function getmenu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, $__edx_site_path__ = null, $__logged_username__ = null, $__logged_email__ = null, $__profile_link__ = null, $__logout_link__ = null, $__account_link__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "edx_site_path" => $__edx_site_path__,
            "logged_username" => $__logged_username__,
            "logged_email" => $__logged_email__,
            "profile_link" => $__profile_link__,
            "logout_link" => $__logout_link__,
            "account_link" => $__account_link__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 27
            echo "  ";
            $context["menus"] = $this;
            // line 28
            echo "  ";
            if (($context["items"] ?? null)) {
                // line 29
                echo "    ";
                if ((($context["menu_level"] ?? null) == 0)) {
                    // line 30
                    echo "      <ul";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => "menu", 1 => "nav", 2 => "navbar-nav"), "method"), "html", null, true));
                    echo ">
    ";
                } else {
                    // line 32
                    echo "      <ul";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => "dropdown-menu"), "method"), "html", null, true));
                    echo ">
    ";
                }
                // line 34
                echo "    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 35
                    echo "      ";
                    // line 36
                    $context["item_classes"] = array(0 => (($this->getAttribute(                    // line 37
$context["item"], "is_expanded", array())) ? ("expanded") : ("")), 1 => ((($this->getAttribute(                    // line 38
$context["item"], "is_expanded", array()) && (($context["menu_level"] ?? null) == 0))) ? ("dropdown") : ("")), 2 => (($this->getAttribute(                    // line 39
$context["item"], "in_active_trail", array())) ? ("active") : ("")));
                    // line 42
                    echo "      ";
                    if (((($context["menu_level"] ?? null) == 0) && $this->getAttribute($context["item"], "is_expanded", array()))) {
                        // line 43
                        echo "        <li";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["item"], "attributes", array()), "addClass", array(0 => ($context["item_classes"] ?? null)), "method"), "html", null, true));
                        echo ">
        <a href=\"";
                        // line 44
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["item"], "url", array()), "html", null, true));
                        echo "\" class=\"dropdown-toggle\" data-target=\"#\" data-toggle=\"dropdown\">";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true));
                        echo " <span class=\"caret\"></span></a>
      ";
                    } else {
                        // line 46
                        echo "        <li";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["item"], "attributes", array()), "addClass", array(0 => ($context["item_classes"] ?? null)), "method"), "html", null, true));
                        echo ">
        ";
                        // line 47
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->getAttribute($context["item"], "title", array()), $this->getAttribute($context["item"], "url", array())), "html", null, true));
                        echo "
      ";
                    }
                    // line 49
                    echo "      ";
                    if ($this->getAttribute($context["item"], "below", array())) {
                        // line 50
                        echo "        ";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", array()), $this->getAttribute(($context["attributes"] ?? null), "removeClass", array(0 => "nav", 1 => "navbar-nav"), "method"), (($context["menu_level"] ?? null) + 1))));
                        echo "
      ";
                    }
                    // line 52
                    echo "      </li>

    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 55
                echo "
   </ul>

<div  id=\"login_tab1\" style=\"display:block;\">
<ol class=\"menu nav navbar-nav\">
<li>

  <form method=\"get\" action=\"";
                // line 62
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>")));
                echo "/courses\" class=\"navbar-form navbar-left\" role=\"search\">
  <div class=\"input-group\">
    <input name=\"search_query\" id=\"search_key_two\" type=\"text\" class=\"form-control\" placeholder=\"Search Courses\">
  <span class=\"input-group-btn\">
  <button type=\"submit\" id=\"search_course\" class=\"btn btn-warning\"><span class=\"icon fa fa-search\" aria-hidden=\"true\"></span></button>
  </div>
</form>

";
                // line 82
                echo "
      </li>

    </ol>
</div>
    <div id=\"login_tab\" style=\"display:block;\">
    <ol class=\"right nav-courseware nav navbar-nav navbar-right\">
            <li class=\"nav-courseware-01\">
              <a type=\"button\" class=\"btn btn-default\" href=\"";
                // line 90
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edx_site_path"] ?? null), "html", null, true));
                echo "/login\">Login</a>
            </li>
           <li class=\"nav-courseware-01\">
              <a type=\"button\" class=\"btn btn-primary\" href=\"";
                // line 93
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edx_site_path"] ?? null), "html", null, true));
                echo "/register\">Register</a>
            </li>
       </ol>
    </div>
  ";
            }
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "themes/iitbombayx/templates/menu--main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  227 => 93,  221 => 90,  211 => 82,  200 => 62,  191 => 55,  183 => 52,  177 => 50,  174 => 49,  169 => 47,  164 => 46,  157 => 44,  152 => 43,  149 => 42,  147 => 39,  146 => 38,  145 => 37,  144 => 36,  142 => 35,  137 => 34,  131 => 32,  125 => 30,  122 => 29,  119 => 28,  116 => 27,  96 => 26,  84 => 116,  80 => 115,  76 => 114,  72 => 113,  62 => 106,  53 => 99,  48 => 24,  45 => 19,  43 => 18,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/iitbombayx/templates/menu--main.html.twig", "/var/www/html/iitbombayx/themes/iitbombayx/templates/menu--main.html.twig");
    }
}
